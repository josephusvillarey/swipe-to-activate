package com.example.swipetoactivate;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MyActivity extends Activity {

    private static final String TAG = MyActivity.class.getSimpleName();

    TextView _view;
    ViewGroup _root;
    private int _xDelta;
    private int _yDelta;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        Button layout_counter = (Button) findViewById(R.id.hello);
        RelativeLayout edge = (RelativeLayout) findViewById(R.id.edge);
        Rect r = new Rect();
        edge.getGlobalVisibleRect(r);

        Log.d(TAG, "rect " + r.left + " " + r.top);

        final FrameLayout.LayoutParams originalParams = (LayoutParams) layout_counter.getLayoutParams();
        final int originalgravity = originalParams.gravity;
        originalParams.gravity = Gravity.CENTER;
        final int originaltopmargin = originalParams.topMargin;
        final int originalleftmargin = originalParams.leftMargin;

        Log.d(TAG, "original top: " + originaltopmargin);
        Log.d(TAG, "original left: " + originalleftmargin);

        layout_counter.setLayoutParams(originalParams);

        // Welcome to fucking motionevent, where everything is in pixels.
        final int pixelDiffForTrigger = (int) dipToPixels(this, 150);
        Log.d(TAG, "pixeldif: " + pixelDiffForTrigger);

        layout_counter.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                // if (currentState != State.EDIT_MOVE) return false;

                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
                if (view.getId() != R.id.hello)
                    return false;

                switch (event.getAction()) {
                case MotionEvent.ACTION_MOVE:
                    params.gravity = originalgravity;
                    params.topMargin = (int) event.getRawY() - view.getHeight();
                    params.leftMargin = (int) event.getRawX() - (view.getWidth() / 2);
                    // Log.d(TAG, "top: " + params.topMargin);
                    // Log.d(TAG, "left: " + params.leftMargin);

                    view.setLayoutParams(params);
                    break;

                case MotionEvent.ACTION_UP:
                    int topMargin = (int) event.getRawY() - view.getHeight();
                    int leftMargin = (int) event.getRawX() - (view.getWidth() / 2);

                    // assume center (x,y) = (0,0), let's get the distance from
                    // the center!
                    // remember the formula for the length of a hypotenuse, h =
                    // sqrt(x^2 + y^2)

                    int distance = (int) Math.sqrt((topMargin * topMargin) + (leftMargin * leftMargin));
                    Log.d(TAG, "topmargin: " + topMargin + " leftmargin: " + leftMargin + " distance: " + distance);
                    if (distance >= pixelDiffForTrigger) {
                        Toast.makeText(MyActivity.this, "trigger!", Toast.LENGTH_SHORT).show();
                    }

                    params.topMargin = originaltopmargin;
                    params.leftMargin = originalleftmargin;
                    params.gravity = Gravity.CENTER;

                    view.setLayoutParams(params);
                    break;

                case MotionEvent.ACTION_DOWN:
                    view.setLayoutParams(params);
                    break;
                }

                return true;
            }
        });

    }

    public static float dipToPixels(Context context, float dipValue) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
    }

}